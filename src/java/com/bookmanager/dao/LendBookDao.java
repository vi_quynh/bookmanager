/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.dao;

import java.util.List;

import com.bookmanager.bean.Book;
import com.bookmanager.bean.LendBook;
import com.bookmanager.bean.Student;

/**
 *
 * @author viquy
 */
public interface LendBookDao {

    @SuppressWarnings("rawtypes")
    List queryLendBook(String Stu_Id, int pageNow, int pageSize);

    int lendBookSize(String Stu_Id);

    void lendBook(LendBook lendBook, Book book, Student student);

    void returnBook(LendBook lendBook);
}
