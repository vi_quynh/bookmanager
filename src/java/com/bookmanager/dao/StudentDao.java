/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.dao;

import java.util.List;

import com.bookmanager.bean.Student;

/**
 *
 * @author viquy
 */
public interface StudentDao {

    @SuppressWarnings("rawtypes")
    List showAllStudents();

    void addStudent(Student student);

    void deleteStudent(String Stu_Id);
}
