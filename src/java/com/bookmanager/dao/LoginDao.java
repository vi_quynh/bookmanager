/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.dao;


import com.bookmanager.bean.Lib_Systemer;
import com.bookmanager.bean.Student;
import com.bookmanager.bean.Sys_Systemer;

/**
 *
 * @author viquy
 */
public interface LoginDao {

    Student ConfirmStudent(String userName, String password);

    Lib_Systemer ConfirmLibSystemer(String userName, String password);

    Sys_Systemer ConfirmSysSystemer(String userName, String password);
}
