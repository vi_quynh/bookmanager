/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.dao;

import java.util.List;

import com.bookmanager.bean.Book;

/**
 *
 * @author viquy
 */
public interface BookDao {
	void addBook(Book book); 
	void deleteBook(String Isbn);
	void updateBook(Book book);
	Book queryBookByName(String bookName);
	List queryBookByNames(String Book_Name);
}
