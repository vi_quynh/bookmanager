/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.until;

/**
 *
 * @author viquy
 */
public class Pager {

    private int pageNow; // current page number
    private int pageSize = 4;
    private int totalPage;
    private int totalSize;
    private boolean hasFirst;
    private boolean hasPre;
    private boolean hasNext;
    private boolean hasLast;

    public Pager(int pageNow, int totalSize) {
        super();
        this.pageNow = pageNow;
        this.totalSize = totalSize;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public int getPageNow() {
        return pageNow;
    }

    public void setPageNow(int pageNow) {
        this.pageNow = pageNow;
    }

    public int getTotalPage() { // 总共页数算法
        totalPage = getTotalSize() / pageSize;
        if (totalSize % pageSize != 0) {
            totalPage++;
        }
        return totalPage;
    }

    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    public boolean isHasFirst() {
        // Nếu hiện là trang đầu tiên, không có trang chủ
        if (pageNow == 1) {
            return false;
        } else {
            return true;
        }
    }

    public void setHasFirst(boolean hasFirst) {
        this.hasFirst = hasFirst;
    }

    public boolean isHasPre() {
        // Nếu có một trang chủ có trang trước, bởi vì có một trang chủ không phải là trang đầu tiên
        if (this.isHasFirst()) {
            return true;
        } else {
            return false;
        }
    }

    public void setHasPre(boolean hasPre) {
        this.hasPre = hasPre;
    }

    public boolean isHasNext() {
        // Nếu có trang cuối cùng có trang tiếp theo, vì trang cuối không phải là trang cuối cùng
        if (isHasLast()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isHasLast() {
        if (pageNow != this.getTotalPage()) {
            return true;
        } else {
            return false;
        }
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public void setHasLast(boolean hasLast) {
        this.hasLast = hasLast;
    }
}
