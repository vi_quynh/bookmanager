/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.action;

import com.bookmanager.bean.Student;
import com.bookmanager.daoImpl.StudentDaoImple;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author viquy
 */
@SuppressWarnings("serial")
public class StudentAction extends ActionSupport {

    private Student student;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String addstudent() {
        System.out.println(student.getStu_Email());
        if (student == null) {
            return ERROR;
        }
        try {
            System.out.println("hello");
            new StudentDaoImple().addStudent(student);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SUCCESS;
    }

}
