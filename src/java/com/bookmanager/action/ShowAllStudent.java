/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.action;

import com.bookmanager.daoImpl.StudentDaoImple;
import static com.opensymphony.xwork2.Action.ERROR;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author viquy
 */
@SuppressWarnings("serial")
public class ShowAllStudent extends ActionSupport {

    public String showAll() {
        List allStudents = new StudentDaoImple().showAllStudents();
        if (allStudents == null) {
            return ERROR;
        }
        ServletActionContext.getRequest().getSession().setAttribute("allStudents", allStudents);
        return SUCCESS;
    }
}
