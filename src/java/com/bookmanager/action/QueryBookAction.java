/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.action;

import com.bookmanager.daoImpl.BookDaoImple;
import com.bookmanager.until.Pager;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author viquy
 */
public class QueryBookAction extends ActionSupport {

    private HttpServletRequest request;
    private ServletActionContext sac;
    private ActionContext ac;
    private BookDaoImple bookDaoImple = new BookDaoImple();
    private String bookName;
    private int pageNow = 1;
    private int pageSize = 4;

    public int getPageNow() {
        return pageNow;
    }

    public void setPageNow(int pageNow) {
        this.pageNow = pageNow;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    @Override
    public String execute() throws Exception {
        if (bookName == null || bookName.length() == 0) {
            ac.getContext().getSession().put("noneBook_Name", "Please enter a book title：");
            return "noneBook_Name";
        }
        List list = bookDaoImple.queryBookFormPages(bookName, pageNow, pageSize);
        Pager page = new Pager(this.getPageNow(), bookDaoImple.booksSize(bookName));
        Map request = (Map) ActionContext.getContext().get("request");
        System.out.println(list.size());
        request.put("list", list);
        request.put("page3", page);
        request.put("bookName", bookName);
        return SUCCESS;
    }
}
