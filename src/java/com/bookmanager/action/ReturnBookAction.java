/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.action;

import com.bookmanager.bean.Student;
import com.bookmanager.daoImpl.LendBookDaoImpl;
import com.bookmanager.until.Pager;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author viquy
 */
public class ReturnBookAction extends ActionSupport {
	private HttpServletRequest request;
	private ServletActionContext sac;
	private ActionContext ac;
	private LendBookDaoImpl lendBookDaoImple = new LendBookDaoImpl();
	private Student student;
	private int pageNow = 1;
	private int pageSize = 4;

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNow() {
		return pageNow;
	}

	public void setPageNow(int pageNow) {
		this.pageNow = pageNow;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}


	@Override
	public String execute() throws Exception {
		if(student.getStu_Id() ==null||student.getStu_Id().length() == 0){
			ac.getContext().getSession().put("noneStu_Id", "Please enter your library card number：");
			return "noneStu_ID";
		}
		List list = lendBookDaoImple.queryLendBook(student.getStu_Id(), this.getPageNow(), pageSize);
		Pager page = new Pager(this.getPageNow(), lendBookDaoImple.lendBookSize(student.getStu_Id()));
		Map request = (Map) ActionContext.getContext().get("request");
		System.out.println(list.size());
		request.put("queryList2", list);
		request.put("page2", page);
		request.put("returnId", student.getStu_Id());
		return SUCCESS;
	}
}

