/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.action;

import com.bookmanager.bean.Book;
import com.bookmanager.bean.LendBook;
import com.bookmanager.bean.Student;
import com.bookmanager.daoImpl.LendBookDaoImpl;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;

/**
 *
 * @author viquy
 */
@SuppressWarnings("serial")
public class LendAction extends ActionSupport {
	private LendBookDaoImpl lendBookDaoImpl = new LendBookDaoImpl();
	private LendBook lendBook;
	private Student student;
	private Book book;
	
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public Book getBook() {
		return book;
	}
	public void setBook(Book book) {
		this.book = book;
	}
	public LendBook getLendBook() {
		return lendBook;
	}
	public void setLendBook(LendBook lendBook) {
		this.lendBook = lendBook;
	}
	@Override
	public String execute() throws Exception {
		System.out.println(student.getStu_Id());
		lendBookDaoImpl.lendBook(lendBook, book, student);
		System.out.println("Library information inserted successfully！");
		return SUCCESS;
	}
}

