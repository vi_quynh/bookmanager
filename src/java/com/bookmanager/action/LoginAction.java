/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.action;

import com.bookmanager.bean.Lib_Systemer;
import com.bookmanager.bean.Student;
import com.bookmanager.bean.Sys_Systemer;
import com.bookmanager.daoImpl.LoginDaoImpl;
import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;

/**
 *
 * @author viquy
 */
@SuppressWarnings("serial")
public class LoginAction extends ActionSupport {

    private LoginDaoImpl loginDaoImpl;
    private Student student;
    private Lib_Systemer libSystemer;
    private Sys_Systemer sysSystemer;
    private String username;
    private String password;
    private String role;
    private ActionContext ac;
    @SuppressWarnings("rawtypes")
    private Map session;
    @SuppressWarnings("rawtypes")
    private Map application;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Lib_Systemer getLibSystemer() {
        return libSystemer;
    }

    public void setLib_Systemer(Lib_Systemer libSystemer) {
        this.libSystemer = libSystemer;
    }

    public Sys_Systemer getSysSystemer() {
        return sysSystemer;
    }

    public void setSysSystemer(Sys_Systemer sysSystemer) {
        this.sysSystemer = sysSystemer;
    }

    public boolean isInvalid(String value) {
        return value == null || value.length() == 0;
    }

    @Override
    public void validate() {
    }

    @SuppressWarnings({"unchecked", "static-access"})
    @Override
    public String execute() throws Exception {
        loginDaoImpl = new LoginDaoImpl();
        application = ac.getContext().getApplication();
        session = ac.getContext().getSession();
        System.out.println(username + password + role);
        if (isInvalid(role)) {
            application.put("tips", "Title selection error");
            return INPUT;
        }
        System.out.println(role);
        if (role.equals("Student")) {
            student = loginDaoImpl.ConfirmStudent(username, password);
            if (student == null) {
                session.put("ErrorMsg", "The student user does not exist");
                return INPUT;
            }
            session.put("stuName", student.getStu_Name());
            return "student";
        } else if (role.equals("Lib_Systemer")) {
            System.out.println(role);
            libSystemer = loginDaoImpl.ConfirmLibSystemer(username, password);
            if (libSystemer == null) {
                session.put("ErrorMsg", "The librarian user does not exist");
                return INPUT;
            }
            session.put("libSystemerName", libSystemer.getLib_Name());
            return "libSystemer";
        } else if (role.equals("Sys_Systemer")) {
            sysSystemer = loginDaoImpl.ConfirmSysSystemer(username, password);
            if (sysSystemer == null) {
                session.put("ErrorMsg", "The system administrator user does not exist");
                return INPUT;
            }
            session.put("sysSystemerName", sysSystemer.getSys_Name());
            return "sysSystemer";
        } else {
            return SUCCESS;
        }
    }
}
