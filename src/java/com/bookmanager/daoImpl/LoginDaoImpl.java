/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.daoImpl;

import com.bookmanager.bean.Lib_Systemer;
import com.bookmanager.bean.Student;
import com.bookmanager.bean.Sys_Systemer;
import com.bookmanager.dao.LoginDao;
import com.bookmanager.until.HibernateSessionFactory;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author viquy
 */
public class LoginDaoImpl implements LoginDao {

    @SuppressWarnings("rawtypes")
    public Student ConfirmStudent(String userName, String password) {
        System.out.println(userName + password);
        if (userName == null && "".equals(userName)) {
            return null;
        }
        Session session = HibernateSessionFactory.getSession();
        Transaction transaction = null;
        Student student = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Student where STU_ID=? "
                    + "and STU_PASSWORD=?");
            query.setParameter(0, userName);
            query.setParameter(1, password);
            System.out.println(userName + password + "test");
            student = (Student) query.uniqueResult();
            if (student == null) {
                return null;
            }
            transaction.commit();
            session.close();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return student;
    }

    @SuppressWarnings("rawtypes")
    public Lib_Systemer ConfirmLibSystemer(String userName, String password) {
        if (userName == null && "".equals(userName)) {
            return null;
        }
        Session session = HibernateSessionFactory.getSession();
        Transaction transaction = null;
        Lib_Systemer lib_Systemer = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Lib_Systemer where LIB_COUNT=? "
                    + "and LIB_PASSWORD=?");
            query.setParameter(0, userName);
            query.setParameter(1, password);
            System.out.println(userName);
            lib_Systemer = (Lib_Systemer) query.uniqueResult();
            if (lib_Systemer == null) {
                return null;
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return lib_Systemer;
    }

    @SuppressWarnings("rawtypes")
    public Sys_Systemer ConfirmSysSystemer(String userName, String password) {
        if (userName == null && "".equals(userName)) {
            return null;
        }
        Session session = HibernateSessionFactory.getSession();
        Transaction transaction = null;
        Sys_Systemer system = null;
        try {
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Sys_Systemer where Sys_Count=? "
                    + "and Sys_Password=?");
            query.setParameter(0, userName);
            query.setParameter(1, password);
            List list = query.list();
            system = (Sys_Systemer) list.get(0);
            if (system == null) {
                return null;
            }
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return system;
    }

}
