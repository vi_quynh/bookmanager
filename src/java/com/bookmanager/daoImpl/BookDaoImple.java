/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.daoImpl;

import com.bookmanager.bean.Book;
import com.bookmanager.dao.BookDao;
import com.bookmanager.until.HibernateSessionFactory;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author viquy
 */
public class BookDaoImple implements BookDao {

    public int booksSize(String BookName) {
        Session session = null;
        Transaction transaction = null;
        int BooksSize = 0;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Book where Book_Name=?");
            query.setParameter(0, BookName);
            BooksSize = query.list().size();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return BooksSize;
    }

    public List queryBookFormPages(String BookName, int pageNow, int pageSize) {
        Session session = null;
        Transaction transaction = null;
        List books = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            Query q = session.createQuery("from Book where Book_Name=?");
            q.setParameter(0, BookName);
            //Paging operation
            q.setFirstResult(pageSize * (pageNow - 1));
            q.setMaxResults(pageSize);
            books = q.list();
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return books;
    }

    public void addBook(Book book) {
        if (book == null) {
            throw new NullPointerException("传入对象为空");
        }
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            session.save(book);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    public void deleteBook(String Isbn) {
        if (Isbn == null && "".equals(Isbn)) {
            throw new NullPointerException("未传入有效参数");
        }
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Book where BOOK_ISBN=?");
            query.setParameter(0, Isbn);
            query.setMaxResults(1);
            Book book = (Book) query.uniqueResult();
            session.delete(book);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void updateBook(Book book) {
        if (book == null) {
            throw new NullPointerException("传入对象为空");
        }
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            session.update(book);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public Book queryBookByName(String bookIsbn) {
        Session session = null;
        Transaction transaction = null;
        Book book = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Book where BOOK_ISBN=?");
            query.setParameter(0, bookIsbn);
            query.setMaxResults(1);
            book = (Book) query.uniqueResult();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        if (book == null) {
            throw new RuntimeException("没有此参数");
        }
        return book;
    }

    /*
	 * Theo tên sách trong dữ liệu truy vấn cơ sở dữ liệu, trả về tất cả các sách có cùng tên
     */
    @SuppressWarnings("rawtypes")
    public List queryBookByNames(String Book_Name) {
        List allBook = null;
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            System.out.println(Book_Name);
            Query query = session.createQuery("from Book where BOOk_NAME=?");
            query.setParameter(0, Book_Name);
            allBook = query.list();
            System.out.println(allBook.size());
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        if (allBook == null) {
            throw new RuntimeException("Without this parameter");
        }
        return allBook;
    }
}
