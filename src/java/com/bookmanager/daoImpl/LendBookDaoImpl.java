/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.daoImpl;

import com.bookmanager.bean.Book;
import com.bookmanager.bean.LendBook;
import com.bookmanager.bean.Student;
import com.bookmanager.dao.LendBookDao;
import com.bookmanager.until.HibernateSessionFactory;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author viquy
 */
public class LendBookDaoImpl implements LendBookDao {

    @SuppressWarnings("rawtypes")
    public List queryLendBook(String Stu_Id, int pageNow, int pageSize) {
        Session session = null;
        Transaction transaction = null;
        List record = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("from LendBook where Stu_ID=?");
            query.setParameter(0, Stu_Id);
            query.setFirstResult(pageSize * (pageNow - 1));
            query.setMaxResults(pageSize);
            record = query.list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return record;
    }

    public int lendBookSize(String Stu_Id) {
        Session session = null;
        Transaction transaction = null;
        int lendSize = 0;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("from LendBook where Stu_ID=?");
            query.setParameter(0, Stu_Id);
            lendSize = query.list().size();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
        return lendSize;
    }

    public void lendBook(LendBook lendBook, Book book, Student student) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            if (student == null || book == null) {
                throw new NullPointerException("Object is empty");
            }
            lendBook.setStudent(student);
            lendBook.setBook(book);
            session.save(lendBook);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }

    }

    public void returnBook(LendBook lendBook) {
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            if (lendBook == null) {
                throw new NullPointerException("Incoming object is empty");
            }
            LendBook lendBook2 = (LendBook) session.get(LendBook.class, lendBook.getLendBook_Id());
            if (lendBook2 == null) {
                System.out.println("Book Id may be wrong");
                throw new RuntimeException("The object is empty");
            }
            session.delete(lendBook2);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

}
