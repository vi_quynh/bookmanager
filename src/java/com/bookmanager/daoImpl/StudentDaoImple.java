/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bookmanager.daoImpl;

import com.bookmanager.bean.Student;
import com.bookmanager.dao.StudentDao;
import com.bookmanager.until.HibernateSessionFactory;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author viquy
 */
public class StudentDaoImple implements StudentDao {

    public void addStudent(Student student) {
        if (student == null) {
            throw new NullPointerException("Incoming object is empty");
        }
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            session.save(student);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void deleteStudent(String Stu_Id) {
        if (Stu_Id == null && "".equals(Stu_Id)) {
            throw new NullPointerException("No valid parameters passed");
        }
        Session session = null;
        Transaction transaction = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            Student student = (Student) session.get(Student.class, Stu_Id);
            session.delete(student);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    @SuppressWarnings("rawtypes")
    public List showAllStudents() {
        Session session = null;
        Transaction transaction = null;
        List list = null;
        try {
            session = HibernateSessionFactory.getSession();
            transaction = session.beginTransaction();
            Query query = session.createQuery("from Student");
            list = query.list();
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
        return list;
    }

}
