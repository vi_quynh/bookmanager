<%-- 
    Document   : StudentsManage
    Created on : May 7, 2018, 8:19:44 PM
    Author     : viquy
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
    <head>
        <style>
            .font1{font-size:13px;}
        </style>
    </head>
    <body>
        <table border="1" width="599" cellspacing="1" class="font1">
            <tr bgcolor="#E9EDF5">
                <td>Book information</td>
            </tr>
            <tr>
                <td height="400" valign="top"><br>
                    <s:if test="#request.selectedBook==null">
                        <table class="font1">
                            <tr>
                                <td width="100">Scholar:</td>
                                <td><s:textfield name="book.Book_Isbn" value=""/></td>
                                <td width="100">Names:</td>
                                <td><s:textfield name="Book_Price" value="" onblur="check(this)"/></td>
                            </tr>
                            <tr>
                                <td width="100">gender:</td>
                                <td><s:textfield name="book.Book_Name" value=""/></td>
                                <td width="100">mailbox:</td>
                                <td><s:textfield name="BookCount" value="" onblur="check(this)"/></td>
                            </tr>
                            <tr>
                                <td width="100">Admission time:</td>
                                <td><s:textfield name="book.Book_Pushing" value=""/></td>
                                <td width="100">graduation time:</td>
                                <td><s:textfield name="Book_Count" value="" disabled=""/></td>
                            </tr>
                        </table>
                    </s:if>
                    <s:else>
                        <s:set name="selectedBook" value="#request.selectedBook"/>
                        <table>
                            <tr>
                                <td width="100">Scholar:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Isbn"/>" name="book.Book_Isbn" readonly/>
                                </td>
                                <td width="100">Names:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Price"/>" name="Book_Price" onblur="check(this)"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="100">gender:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Name"/>" name="book.Book_Name"/>
                                </td>
                                <td width="100">mailbox:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Count"/>" onblur="check(this)"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="100">Admission time:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Pushing"/>" name="book.Book_Pushing"/>
                                </td>
                                <td width="100">Graduation time:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Count"/>" name="Book_Count" onblur="check(this)"/>
                                </td>
                            </tr>
                        </table>
                    </s:else> 
                </td>
            </tr>
        </table>
    </body>
</html>