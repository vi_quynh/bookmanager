<%-- 
    Document   : StudentsManage
    Created on : May 7, 2018, 8:19:44 PM
    Author     : viquy
--%>
<%@ page language="java" pageEncoding="gb2312"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
    <head>
        <link href="css/font_hover.css" rel="stylesheet" type="text/css">
        <title>Library Management System</title>
        <style>
            .font1{font-size:13px;}
        </style>
        <script language="javascript">
            function check(thisObject) {
                var sTmp = "";
                sTmp = thisObject.value;
                if (isNaN(sTmp)) {
                    alert("Please enter the number");
                    thisObject.select();
                }
            }
        </script>
    </head>
    <body>
        <table id="tableBGcolor" align="center" class="font1">
            <tr>
                <td colspan="2"><jsp:include page="headForSysSystemer.jsp"/></td>
            </tr>
            <tr>
                <s:form id="f1" theme="simple" action="student" method="post" enctype="multipart/form-data" validate="true">
                    <td><jsp:include page="studentSelect.jsp"/></td>
                    <td><jsp:include page="studentInfo.jsp"/></td>
                </s:form>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    FPT University&nbsp;&nbsp;Code��210097<br>2010-2015
                </td>
            </tr>
        </table>
    </body>
</html>
