<%-- 
    Document   : StudentsManage
    Created on : May 7, 2018, 8:19:44 PM
    Author     : viquy
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
    <head>
        <style>
            .font1{font-size:13px;}
        </style>
    </head>
    <body>
        <table  border="1" align="center" width="570" cellspacing="0"  bgcolor="#71CABF" class="font1">
            <tr bgcolor="#E9EDF5">
                <th>Identification ID</th><th>Title </th><th>Publisher</th><th>Price</th><th>Booking time</th><th> Also Book time</th>
                <th>ISBN</th></tr>
                    <s:iterator value="#request.queryList1" id="lend">
                <tr>
                    <td><s:property value="#lend.LendBook_Id"/></td>
                    <td><s:property value="#lend.LendBook_Name"/></td>
                    <td><s:property value="#lend.LendBook_Pushing"/></td>
                    <td><s:property value="#lend.LendBook_Price"/></td>
                    <td><s:date name="#lend.LendBook_Time" format="yyyy-MM-dd"/></td>
                    <td><s:date name="#lend.LendBook_Return" format="yyyy-MM-dd"/></td>
                    <td><s:property value="#lend.book.Book_Isbn" /></td>
                </tr>
            </s:iterator>
        </table>

    </body>
</html>