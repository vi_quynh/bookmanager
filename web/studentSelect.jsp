<%-- 
    Document   : StudentsManage
    Created on : May 7, 2018, 8:19:44 PM
    Author     : viquy
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
    <head>
        <style>
            .font1{font-size:13px;
            }
            #font1{
                margin-left: 160px;
            }
            #submitbutton:HOVER{
                background-color: #00FFFF;
            }
        </style>
    </head>
    <body>
        <table border="1" width="200" cellspacing=1 class="font1" id="font1">
            <tr bgcolor="#E9EDF5">
                <td>Function selection：</td>
            </tr>
            <tr>
                <td align="center" valign="top" height="400">
                    <br><s:submit value="Student information display" method="displayAllStudents" id="submitbutton"/><br>
                    <br><s:submit value="Add student information" method="addStudentInfo" id="submitbutton" /><br>
                    <br><s:submit value="Delete student information" method="deleteStudentInfo" id="submitbutton" /><br>
                    <br><s:submit value="（Backup button）" method="" id="submitbutton"/><br>
                </td>
            </tr>
        </table>
    </body>
</html>