<%-- 
    Document   : StudentsManage
    Created on : May 7, 2018, 8:19:44 PM
    Author     : viquy
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
    <head>
        <link href="css/font_hover.css" rel="stylesheet" type="text/css">
        <title>Library Management System - Librarian Interface</title>
    </head>
    <body>
        <table id="tableBGcolor" align="center">
            <tr>
                <td colspan="2"><jsp:include page="headForLibSystemer.jsp"/></td>
            </tr>
            <tr>
                <td height="400"></td>
            </tr>
            <tr>
                <td align="center">
                    <font color="red" size="10">Show book - admin info here</font>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <font size="2">FPT University&nbsp;&nbsp;Zip Code: 610106<br> Librarian Interface</font>
                </td>
            </tr>
        </table>
    </body>
</html>
