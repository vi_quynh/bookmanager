<%-- 
    Document   : StudentsManage
    Created on : May 7, 2018, 8:19:44 PM
    Author     : viquy
--%>
<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<html>
    <head>
        <link href="css/font_hover.css" rel="stylesheet" type="text/css">
        <title>Library Management System - Book Search</title>
        <style>
            .font1{font-size:13px;}
        </style>
    </head>
    <body>
        <table id="tableBGcolor" align="center" >
            <tr>
                <td colspan="2"><jsp:include page="headForLibSystemer.jsp"/></td>
            </tr>
            <tr>
                <td><jsp:include page="searchLendInfo.jsp"/></td>
                <td><jsp:include page="lendBook.jsp"/></td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="font1">
                    FPT University&nbsp;&nbsp;Postcode: 610106<br> @Librarian Interface
                </td>
            </tr>
        </table>
    </body>
</html>
