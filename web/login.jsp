<%-- 
    Document   : StudentsManage
    Created on : May 7, 2018, 8:19:44 PM
    Author     : viquy
--%>
<%@ page language="java" import="java.util.*"
         contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title>loginInterface</title>
        <link rel="stylesheet" type="text/css" href="css/Login_style.css">
    </head>
    <body>
        <img id="image_LibTop" src="image/LibraryTop.png" alt="图书信息管理系统">
        <div id="loginITC">
            <s:form action="login" method="post" theme="simple">
                <br>
                Account&nbsp;&nbsp;number:<s:textfield name="username"></s:textfield>
                    <br>
                    <div id="ErrorMsg">&nbsp;&nbsp;&nbsp;<s:property value="#session.ErrorMsg" />
                </div>
                <br>
                Secret&nbsp;&nbsp;code:<s:textfield name="password" type="password"></s:textfield>
                    <br>
                    <br>
                <s:radio id="roleList" list="{'Student':'student','Lib_Systemer':'librarian','Sys_Systemer':'System administrator' }"
                         name="role" value="1"></s:radio>
                    <br>
                    <br>
                <s:submit value="log in" ></s:submit>&nbsp;&nbsp;
                <s:reset value="Reset"></s:reset>
            </s:form>
        </div>
    </body>
</html>
