<%-- 
    Document   : bookInfo
    Created on : May 8, 2018, 7:40:56 AM
    Author     : viquy
--%>

<%@ page language="java" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <style>
            .font1{font-size:13px;}
        </style>
    </head>
    <body>
        <table border="1" width="599" cellspacing="1" class="font1">
            <tr bgcolor="#E9EDF5">
                <td>Book information</td>
            </tr>
            <tr>
                <td height="400" valign="top"><br>
                    <s:if test="#request.selectedBook==null">
                        <table class="font1">
                            <tr>
                                <td width="100">ISBN:</td>
                                <td><s:textfield name="book.Book_Isbn" value=""/></td>
                                <td width="100">price:</td>
                                <td><s:textfield name="Book_Price" value="" onblur="check(this)"/></td>
                            </tr>
                            <tr>
                                <td width="100">Title:</td>
                                <td><s:textfield name="book.Book_Name" value=""/></td>
                                <td width="100">Replica quantity:</td>
                                <td><s:textfield name="BookCount" value="" onblur="check(this)"/></td>
                            </tr>
                            <tr>
                                <td width="100">Publisher:</td>
                                <td><s:textfield name="book.Book_Pushing" value=""/></td>
                                <td width="100">inventory:</td>
                                <td><s:textfield name="Book_Count" value="" disabled=""/></td>
                            </tr>
                            <tr>
                                <td width="100">Writer:</td>
                                <td><s:textfield name="book.Book_Editor" value=""/></td>
                            </tr>
                            <tr>
                                <td valign="top">Executive summary:</td>
                                <td><s:textarea cols="20" rows="6" value="" name="book.Book_Content"/></td>
                                <td colspan="2" align="center">
                                    <img id="image" src="" width="100" height="120"/>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="1" align="right">cover image:</td>
                                <td colspan="2">
                                    <s:file name="photo" accept="image/*" onchange="document.all['image'].src = this.value;"/>
                                </td>
                            </tr>
                        </table>
                    </s:if>
                    <s:else>
                        <s:set name="selectedBook" value="#request.selectedBook"/>
                        <table>
                            <tr>
                                <td width="100">ISBN:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Isbn"/>" name="book.Book_Isbn" readonly/>
                                </td>
                                <td width="100">price:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Price"/>" name="Book_Price" onblur="check(this)"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="100">Title:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Name"/>" name="book.Book_Name"/>
                                </td>
                                <td width="100">Replica quantity:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Count"/>" name="" onblur="check(this)"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="100">Publisher:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Pushing"/>" name="book.Book_Pushing"/>
                                </td>
                                <td width="100">inventory:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Count"/>" name="Book_Count" onblur="check(this)"/>
                                </td>
                            </tr>
                            <tr>
                                <td width="100">Writer:</td>
                                <td>
                                    <input type="text" value="<s:property value="#selectedBook.Book_Editor"/>" name="book.Book_Editor"/>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">Executive summary:</td>
                                <td>
                                    <textarea rows="6" cols="20" name="book.Book_Content">
                                        <s:property value="#selectedBook.Book_Content"/>
                                    </textarea>
                                </td>
                                <td colspan="2" align="center">
                                    <img id="image" src="getImage.action?book.Book_Isbn=<s:property value="#selectedBook.Book_Isbn"/>" width="100" height="120">
                                </td>	
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td colspan="1" align="right">cover image:</td>
                                <td colspan="2">
                                    <s:file name="photo"  accept="image/*" onchange="document.all['image'].src = this.value;"/>
                                </td>
                            </tr>
                        </table>
                    </s:else> 
                </td>
            </tr>
        </table>
    </body>
</html>
