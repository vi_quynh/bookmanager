<%-- 
    Document   : StudentsManage
    Created on : May 7, 2018, 8:19:44 PM
    Author     : viquy
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Student Information</title>
    </head>
    <body>
        <s:form action="lxq" method="post">
            <s:textfield label="StudentID:" name="student.Stu_Id"/>
            <s:password label="StudentPassword:" name="student.Stu_Password"/>
            <s:textfield label="StudentName：" name="student.Stu_Name"/>
            <%-- <s:radio list="#{'male':'boy' ,'female':'girl'}" label="Student Gender" name="student.Stu_Sex"/>--%>
            <s:radio list="{'male':'boy' ,'female':'girl'}" label="Student Gender" name="student.Stu_Sex"/>
            <s:textfield label="StudentGender：" name="student.Stu_Sex"/>
            <s:textfield label="StudentMail" name="student.Stu_Email"/>
            <s:textfield label="Student Regist Date(yyyy-MM-dd):" name="student.Stu_Regist_Date"/>
            <s:textfield label="Student Graduate Date(yyyy-MM-dd):" name="student.Stu_Graduate_Date"/>
            <s:submit value="Submit"/>
        </s:form>
    </body>
</html>